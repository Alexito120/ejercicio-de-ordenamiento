package ejerccicioordenamiento;
import javax.swing.JOptionPane;
public class MenuPrincipal {
      public static void main(String[] args) {
      
        int a;
        int salir=0;
             do{
                try{
                a=Integer.parseInt(JOptionPane.showInputDialog("Menu de ordenamientos\n1-Ordenamiento por burbuja\n2-Ordenamiento Shell\n3-Ordenamiento QuickSort\n4-Ordenamiento por objetos\n5-Salir del menu"));
                switch (a){
                case 1:
                OrdenamientoBurbuja burbuja =new OrdenamientoBurbuja();
                burbuja.metodoBurbuja();
                break;
                case 2:
                OrdenamientoShell shell =new OrdenamientoShell();
                shell.metodoShell();
                break;
                case 3:
                OrdenamientoQuicksort quickshort =new OrdenamientoQuicksort();
                quickshort.metodoQuicksort();
                break;
                case 4:
                OrdenamientoObjetos arreglos =new OrdenamientoObjetos();
                arreglos.metodoObjetos();
                break;
                case 5:
                salir=1;
                break;
                default:
                JOptionPane.showMessageDialog(null,"Ingrese un valor valido");
                break;
    }
                }catch(Exception ex){
                JOptionPane.showMessageDialog(null,"Digitar una opcion valida");
                 }
             }while(salir==0);
    } 
    
}
