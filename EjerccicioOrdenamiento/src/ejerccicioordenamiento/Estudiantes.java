
package ejerccicioordenamiento;

public class Estudiantes {
    
    private String nombre;
    private String id;
    private double nota;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }
    
    public Estudiantes(String nombre, String id,int nota){
        setNombre(nombre);
        setId(id);
        setNota(nota);
    }
    public String imprimir (){
        String mensaje = "";
        mensaje = getNombre()+" "+getId()+" "+getNota();
        return mensaje;
     
        
    }
    
}
